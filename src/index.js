import angular from 'angular';
import ngAnimate from 'angular-animate';
import ngMessages from 'angular-messages';
import ngMaterial from 'angular-material';

import {servicesModule} from './app/services/index';
import {componentsModule} from './app/components/index';

import {main} from './app/main';
import {header} from './app/header';
import {footer} from './app/footer';

import './index.scss';

angular
  .module('app', [servicesModule, componentsModule, ngMessages, ngAnimate, ngMaterial])
  .component('app', main)
  .component('ryanHeader', header)
  .component('ryanFooter', footer);
