module.exports = {
  extends: [
    'angular'
  ],
  rules: {
    'angular/no-service-method': 0,
    'no-debugger': 0,
    'linebreak-style': 0,
    'angular/log': 0
  }
}
