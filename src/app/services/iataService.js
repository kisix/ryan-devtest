export class IataService {
  constructor($http, $q) {
    this.$http = $http;
    this.$q = $q;
    this.apiEndpoint = 'https://murmuring-ocean-10826.herokuapp.com/en/api/2/forms/flight-booking-selector/';
    this.iataCodes = {};
    this.success = false;
  }

  getIataCodes() {
    if (this.success) {
      return this.$q.when(this.iataCodes);
    }
    return this.$http
      .get(this.apiEndpoint)
      .then(response => {
        this.iataCodes = response.data;
        this.success = true;
        return this.iataCodes;
      });
  }
}
