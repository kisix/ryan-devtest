import angular from 'angular';

import {IataService} from './iataService';
import {CheapFlightService} from './cheapFlightService';

export const servicesModule = 'services';

angular
  .module(servicesModule, [])
  .service('iataService', IataService)
  .service('cheapFlightService', CheapFlightService);
