export class CheapFlightService {
  constructor($http) {
    this.subscribers = [];
    this.$http = $http;
    this.apiUrl = 'https://murmuring-ocean-10826.herokuapp.com/en/api/2/flights';
  }

  getFlights(origin, destination, startDate, endDate) {
    const query = `${this.apiUrl}/from/${origin}/to/${destination}/${startDate}/${endDate}/250/unique/?limit=15&offset-0`;
    return this.$http
      .get(query)
      .then(response => {
        this.notify(response.data.flights);
        return response.data.flights;
      });
  }

  subscribe(callback) {
    if (!this.subscribers.includes(callback)) {
      this.subscribers.push(callback);
    }
  }

  notify(data) {
    this.subscribers.forEach(curr => {
      curr(data);
    });
  }
}
