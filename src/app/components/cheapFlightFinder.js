class cheapFlightFinderController {
  constructor(iataService) {
    iataService.getIataCodes()
    .then(response => {
      this.iataData = response;
    });
  }
}

export const cheapFlightFinder = {
  template: require('./cheapFlightFinder.html'),
  controller: cheapFlightFinderController
};
