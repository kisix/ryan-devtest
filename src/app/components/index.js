import angular from 'angular';

import {cheapFlightFinder} from './cheapFlightFinder';
import {cheapFlightSearch} from './cheapFlightSearch';
import {cheapFlightResult} from './cheapFlightResult';

export const componentsModule = 'components';
angular
  .module(componentsModule, [])
  .component('cheapFlightFinder', cheapFlightFinder)
  .component('cheapFlightSearch', cheapFlightSearch)
  .component('cheapFlightResult', cheapFlightResult);
