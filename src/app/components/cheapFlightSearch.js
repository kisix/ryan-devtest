class CheapFlightSearchController {
  constructor(cheapFlightService) {
    this.cheapFlightService = cheapFlightService;
    this.getIataCode = code => {
      let result = {};
      if (angular.isObject(this.iataData) && angular.isArray(this.iataData.airports)) {
        result = this.iataData.airports.find(curr => {
          return angular.isString(code) &&
            (curr.iataCode.toLowerCase() === code.toLowerCase() || curr.name.toLowerCase() === code.toLowerCase());
        }) || {};
      }
      return result.iataCode;
    };
  }

  searchFlight() {
    delete this.cheapestFlight;
    delete this.flights;
    const origin = this.getIataCode(this.origin);
    const destination = this.getIataCode(this.destination);
    const fromDate = this.fromDate.toISOString().match(/\d{4}-\d{2}-\d{2}/)[0];
    const toDate = this.toDate.toISOString().match(/\d{4}-\d{2}-\d{2}/)[0];

    if (origin && destination) {
      this.cheapFlightService.getFlights(origin, destination, fromDate, toDate)
        .then(response => {
          this.flights = response;
        });
    }
  }
}

export const cheapFlightSearch = {
  template: require('./cheapFlightSearch.html'),
  bindings: {
    origin: "<",
    destination: "<",
    fromDate: "<",
    toDate: "<",
    iataData: "<",
    flights: "="
  },
  controller: CheapFlightSearchController
};
