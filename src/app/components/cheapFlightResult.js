class CheapFlightResultController {

  constructor(cheapFlightService) {
    this.moreResults = false;
    cheapFlightService.subscribe(flights => {
      this.flights = flights;
      this.cheapestFlight = this.getCheapestFlight(flights);
    });
  }

  getCheapestFlight(flights) {
    if (angular.isArray(flights) && flights.length > 0) {
      return (flights).reduce((prev, curr) => {
        if (angular.isUndefined(prev)) {
          return curr;
        }
        return prev.price < curr.price ? prev : curr;
      });
    }
  }

  noResult() {
    return angular.isArray(this.flights) && this.flights.length === 0;
  }
}

export const cheapFlightResult = {
  template: require('./cheapFlightResult.html'),
  controller: CheapFlightResultController
};
